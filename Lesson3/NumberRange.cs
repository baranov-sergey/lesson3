﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class NumberRange
    {
        public static void NumberInRange()
        {
            while (true)
            {
                Console.WriteLine("Please enter a number from 0 to 100");
                if (int.TryParse(Console.ReadLine(), out int number) == false)
                { Console.WriteLine("Incorrect number, please re-enter the operation"); continue; }
                if (number >= 0 && number <= 14)
                {
                    Console.WriteLine("0 - 14"); 
                }
                else if (number >= 15 && number <= 35)
                {
                    Console.WriteLine("15 - 35"); 
                }
                else if (number >= 36 && number <= 49)
                {
                    Console.WriteLine("36 - 49"); 
                }
                else if (number >= 50 && number <= 100)
                {
                    Console.WriteLine("50 - 100"); 
                }
                else
                {
                    Console.WriteLine("Invalid number, please try again"); continue;
                }
                Console.WriteLine("Let's continue, yes or no?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "yes") { continue; }
                else if (answer == "no") { break; }
                else { Console.WriteLine("Incorrect answer"); }
            }
        }
    }
}
