﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class SimpleCalculator
    {
        public static void Calculator()
        {
            Console.WriteLine("Simple calculator v.1.0");
            while (true)
            {
                Console.WriteLine("Enter the first number");
                if (double.TryParse(Console.ReadLine(), out double operand1) == false)
                { Console.WriteLine("Incorrect number, please re-enter the operation"); continue; }
                Console.WriteLine("Enter the second number");
                if (double.TryParse(Console.ReadLine(), out double operand2) == false)
                { Console.WriteLine("Incorrect number, please re-enter the operation"); continue; }
                Console.WriteLine("Indicate the sign of the operation(+,-,*,/)");
                string sign = Console.ReadLine();
                switch (sign)
                {
                    case "+": Console.WriteLine(operand1 + operand2); break;
                    case "-": Console.WriteLine(operand1 - operand2); break;
                    case "*": Console.WriteLine(operand1 * operand2); break;
                    case "/":
                        if (operand2 == 0)
                        {
                            Console.WriteLine("Divide by zero is not allowed, please re-enter the operation"); continue;
                        }
                        Console.WriteLine(operand1 / operand2); break;
                    default: Console.WriteLine("Invalid character entered, please try again"); continue;
                }
                Console.WriteLine("Let's continue, yes or no?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "yes") { continue; }
                else if (answer == "no") { break;}
                else { Console.WriteLine("Incorrect answer"); }
             }
        }
    }
}
