﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class ParityCheck
    {
        public static void ParityCheckOption1()
        {
            Console.WriteLine("Parity check v1.0");
            while (true)
            {
                Console.WriteLine("Enter the number");
                if (int.TryParse(Console.ReadLine(), out int number) == false)
                { Console.WriteLine("Incorrect number, please re-enter the operation"); continue; }
                if (number % 2 == 0)
                {
                    Console.WriteLine("parity number");
                }
                else
                {
                    Console.WriteLine("odd number");
                }
                Console.WriteLine("Let's continue, yes or no?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "yes") { continue; }
                else if (answer == "no") { break; }
                else { Console.WriteLine("Incorrect answer"); }
            }
        }
        public static void ParityCheckOption2()
        {
            Console.WriteLine("Parity check v2.0");
            while (true)
            {
                Console.WriteLine("Enter the number");
                if (int.TryParse(Console.ReadLine(), out int number) == false)
                { Console.WriteLine("Incorrect number, please re-enter the operation"); continue; }
                if ((number & 1) == 0)
                {
                    Console.WriteLine("parity number");
                }
                else
                {
                    Console.WriteLine("odd number");
                }
                Console.WriteLine("Let's continue, yes or no?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "yes") { continue; }
                else if (answer == "no") { break; }
                else { Console.WriteLine("Incorrect answer"); }
            }
        }
    }
}
