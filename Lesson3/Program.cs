﻿using Lesson3;

internal class Program
{
    private static void Main(string[] args)
    {
        //Task1 - alculator
        SimpleCalculator.Calculator();

        //Task2 - NumberRange
        NumberRange.NumberInRange();

        //Task3 - WeatherTranslator
        WeatherTranslator.WeatherTranslatorRussianToEnglish();

        //Task4 - ParityCheck, Option1 and Option2
        ParityCheck.ParityCheckOption1();
        ParityCheck.ParityCheckOption2();
    }
}