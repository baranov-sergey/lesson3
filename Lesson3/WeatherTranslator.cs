﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public class WeatherTranslator
    {
        public static void WeatherTranslatorRussianToEnglish()
        {
            Console.WriteLine("Weather translator from Russian to English v1.0");
            while (true)
            {
                Console.WriteLine("What is the weather like today?");
                string weather = (Console.ReadLine()).ToLower();
                switch (weather)
                {
                    case "холодно": Console.WriteLine( "сold"); break;
                    case "прохладно": Console.WriteLine("cool"); break;
                    case "жарко": Console.WriteLine("hot"); break;
                    case "тепло": Console.WriteLine("warm"); break;
                    case "солнечно": Console.WriteLine("sunny"); break;
                    case "облачно": Console.WriteLine("cloudy"); break;
                    case "дождливо": Console.WriteLine("rainy"); break;
                    case "снежно": Console.WriteLine("snowy"); break;
                    case "туманно": Console.WriteLine("foggy"); break;
                    case "ветрено": Console.WriteLine("windy"); break;
                    default:
                        Console.WriteLine("I do not know this weather."); break;
                }
                Console.WriteLine("Let's continue, yes or no?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "yes") { continue; }
                else if (answer == "no") { break; }
                else { Console.WriteLine("Incorrect answer"); }
            }

        }
    }
}
